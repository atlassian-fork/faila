package com.atlassian.faila.cloud.aws

import com.amazonaws.AmazonClientException
import com.amazonaws.services.autoscaling.AmazonAutoScalingAsync
import com.amazonaws.services.autoscaling.model.AutoScalingGroup
import com.amazonaws.services.autoscaling.model.CreateOrUpdateTagsRequest
import com.amazonaws.services.autoscaling.model.DeleteTagsRequest
import com.amazonaws.services.autoscaling.model.DescribeAutoScalingGroupsRequest
import com.amazonaws.services.autoscaling.model.Instance
import com.amazonaws.services.autoscaling.model.Tag
import com.amazonaws.services.autoscaling.model.TagDescription
import com.amazonaws.services.ec2.AmazonEC2
import com.amazonaws.services.ec2.model.TerminateInstancesRequest
import com.atlassian.faila.cloud.CloudManager
import com.atlassian.faila.exceptions.AsgNotFoundException
import com.atlassian.faila.exceptions.EmptyAsgException
import com.atlassian.faila.exceptions.PerformFailaException
import org.slf4j.LoggerFactory
import org.slf4j.MDC
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.util.Random

@Component
class AWSCloudManager @Autowired constructor(
    private val autoScalingClient: AmazonAutoScalingAsync,
    private val eC2Client: AmazonEC2
) : CloudManager {

    companion object {
        private val LOG = LoggerFactory.getLogger(AWSCloudManager::class.java)
        const val FAILA_TAG_PREFIX = "faila:"
        const val ASG_RESOURCE_TYPE = "auto-scaling-group"
    }

    override fun getRandomInstanceFromAsgs(asgs: Set<String>): String {
        val asg = asgs.toList().random()
        MDC.put("ASG", asg)

        LOG.info("Getting random instance to perform faila")
        val group: AutoScalingGroup
        try {
            group = getAsgById(asg)
        } catch (amazonClientException: AmazonClientException) {
            throw PerformFailaException("Failed to get ASG instances from AWS", amazonClientException)
        }

        return getRandomInstance(group).instanceId.also {
            MDC.put("instanceId", it)
        }
    }

    override fun terminateInstance(instanceId: String) {
        LOG.info("Terminating instance")
        try {
            val terminateRequest = TerminateInstancesRequest().apply {
                setInstanceIds(listOf(instanceId))
            }
            eC2Client.terminateInstances(terminateRequest)
        } catch (amazonClientException: AmazonClientException) {
            throw PerformFailaException("Failed to terminate instance", amazonClientException)
        }
    }

    override fun getTaggedAsgIds(): List<String> {
        LOG.debug("Looking for tagged ASGs")
        val asgList: List<AutoScalingGroup>

        try {
            asgList = autoScalingClient.describeAutoScalingGroups(
                DescribeAutoScalingGroupsRequest()
            ).autoScalingGroups
        } catch (amazonClientException: AmazonClientException) {
            LOG.error("Failed to get ASG list from AWS" , amazonClientException)
            return emptyList()
        }

        return asgList
            .filter { getFailaTag(it.tags) != null }
            .map { it.autoScalingGroupName }
            .also {
                LOG.debug("${it.size} tagged ASGs found")
            }
    }

    override fun putTag(asgs: Set<String>, tag: Pair<String, String>) {
        LOG.debug("Tagging ASGs '$asgs' with tag key: '${tag.first}' and value: '${tag.second}'")
        val tagsToPut = tagsForAsgs(asgs) {
            Tag()
                .withKey(tag.first)
                .withValue(tag.second)
                .withResourceId(it)
                .withResourceType(ASG_RESOURCE_TYPE)
                .withPropagateAtLaunch(true)
        }
        val createOrUpdateTagsRequest = CreateOrUpdateTagsRequest()
            .withTags(tagsToPut)
        autoScalingClient.createOrUpdateTags(createOrUpdateTagsRequest)
    }

    override fun deleteTag(asgs: Set<String>, key: String) {
        LOG.debug("Deleting tag '$key' from ASGs '$asgs'")
        val tagsToDelete = tagsForAsgs(asgs) {
            Tag()
                .withKey(key)
                .withResourceId(it)
                .withResourceType(ASG_RESOURCE_TYPE)
        }
        autoScalingClient.deleteTags(
            DeleteTagsRequest().withTags(tagsToDelete)
        )
    }

    override fun getFailaTag(asgs: Set<String>): Pair<String, String>? =
        asgs.mapNotNull {
            getFailaTag(getAsgById(it).tags)
        }.toSet().elementAtOrElse(0) {
            LOG.info("Faila parameters differs between ASGs, it's probably an update happening")
            null
        }

    override fun getFailaTag(asg: String): Pair<String, String>? = getFailaTag(getAsgById(asg).tags)

    private fun getFailaTag(tags: List<TagDescription>): Pair<String, String>? =
        tags.firstOrNull { it.key.startsWith(FAILA_TAG_PREFIX) }?.let { Pair(it.key, it.value) }

    private fun tagsForAsgs(asgs: Set<String>, tagFactory: (String) -> Tag): List<Tag> = asgs.map(tagFactory)

    private fun getAsgById(asgId: String): AutoScalingGroup {
        LOG.debug("Getting ASG by id: '$asgId'")
        val request = DescribeAutoScalingGroupsRequest()
        request.setAutoScalingGroupNames(listOf(asgId))
        val asgList: List<AutoScalingGroup> = autoScalingClient.describeAutoScalingGroups(request).autoScalingGroups
        return asgList.elementAtOrElse(0) {
            LOG.warn("Failed to find ASG '$asgId'")
            throw AsgNotFoundException()
        }
    }

    private fun getRandomInstance(asg: AutoScalingGroup): Instance = when {
        asg.instances.isEmpty() -> {
            LOG.warn("No instances in ASG '${asg.autoScalingGroupName}'")
            throw EmptyAsgException()
        }
        else -> asg.instances.random()
    }

    private fun <E> List<E>.random(): E = get(Random().nextInt(size))

}
