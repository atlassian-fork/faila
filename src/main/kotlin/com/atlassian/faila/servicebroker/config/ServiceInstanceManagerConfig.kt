package com.atlassian.faila.servicebroker.config

import com.atlassian.faila.cloud.CloudManager
import com.atlassian.faila.servicebroker.instances.ServiceInstanceManager
import com.atlassian.faila.servicebroker.instances.dynamo.DynamoServiceInstanceManager
import com.atlassian.faila.servicebroker.instances.dynamo.FailaDAO
import com.atlassian.faila.servicebroker.instances.tags.TagsServiceInstanceManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class ServiceInstanceManagerConfig {

    @Value("\${faila.instance.manager.backend:}")
    private val failaInstanceManagerBackend: String? = null

    @Autowired
    private lateinit var failaDAO: FailaDAO

    @Autowired
    private lateinit var cloudManager: CloudManager

    @Bean
    open fun serviceInstanceManager(): ServiceInstanceManager = when (failaInstanceManagerBackend) {
        "dynamo" -> DynamoServiceInstanceManager(failaDAO)
        else -> TagsServiceInstanceManager(cloudManager)
    }

}
