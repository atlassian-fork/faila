package com.atlassian.faila.servicebroker.service

import com.atlassian.faila.model.Faila
import org.everit.json.schema.Schema
import org.everit.json.schema.ValidationException
import org.everit.json.schema.loader.SchemaLoader
import org.json.JSONObject
import org.json.JSONTokener
import org.quartz.CronExpression
import org.springframework.cloud.servicebroker.exception.ServiceBrokerInvalidParametersException
import org.springframework.stereotype.Component
import java.io.InputStream

@Component
class ParametersValidator {
    private val schemaStream: InputStream = ParametersValidator::class.java.getResourceAsStream("/schemas/faila.json")
    private val rawSchema: JSONObject = JSONObject(JSONTokener(schemaStream))
    private val schema: Schema = SchemaLoader.load(rawSchema)

    fun validate(parameters: Map<String, Any>?): Map<String, Any> {
        val result = validateNotNull(parameters)
        validateSchema(result)
        val faila: Faila = Faila.fromMap(result)
        validateSchedule(faila.schedule)
        return result
    }

    private fun validateNotNull(parameters: Map<String, Any>?): Map<String, Any> = when (parameters) {
        null -> throw ServiceBrokerInvalidParametersException("'parameters' section is mandatory")
        else -> parameters
    }

    private fun validateSchema(parameters: Map<String, Any>) {
        val jsonParameters = JSONObject(JSONTokener(JSONObject(parameters).toString()))
        try {
            schema.validate(jsonParameters)
        } catch (validationException: ValidationException) {
            throw ServiceBrokerInvalidParametersException(validationException.allMessages.joinToString("\n"))
        }
    }

    private fun validateSchedule(cronExpession: String) {
        if (!CronExpression.isValidExpression(cronExpession)) {
            throw ServiceBrokerInvalidParametersException("'schedule' parameter is not valid cron expression")
        }
    }

}
