package com.atlassian.faila.servicebroker.instances.dynamo

import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException
import com.atlassian.faila.exceptions.OutdatedScheduledFailaException
import com.atlassian.faila.model.Faila
import com.atlassian.faila.servicebroker.instances.ServiceInstanceManager
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.servicebroker.exception.ServiceInstanceDoesNotExistException
import java.util.Optional

open class DynamoServiceInstanceManager @Autowired constructor(
    private val failaDAO: FailaDAO
) : ServiceInstanceManager() {

    companion object {
        private val LOG = LoggerFactory.getLogger(DynamoServiceInstanceManager::class.java)
    }

    override fun createFaila(serviceInstanceId: String, parameters: Map<String, Any>): Faila {
        LOG.info("Creating new faila resource for $serviceInstanceId, parameters: $parameters")
        val faila = failaFromMap(serviceInstanceId, parameters)
        LOG.info("Saving new faila resource to db")
        return failaDAO.save(faila)
    }

    override fun deleteFaila(serviceInstanceId: String) {
        LOG.info("Deleting faila resource for '$serviceInstanceId'")
        findFaila(serviceInstanceId)?.let {
            failaDAO.delete(it)
        } ?: run {
            LOG.info("Faila '$serviceInstanceId' does not exists, so nothing to delete")
        }
    }

    override fun updateFaila(serviceInstanceId: String, parameters: Map<String, Any>): Faila {
        findFaila(serviceInstanceId)?.let {
            val updatedFaila = failaFromMap(serviceInstanceId, parameters)
            if (it == updatedFaila) {
                return it
            }

            failaDAO.delete(it)
            failaDAO.save(updatedFaila)
            return updatedFaila
        } ?: run {
            LOG.error("Couldn't update service instance '$serviceInstanceId', it doesn't exist")
            throw ServiceInstanceDoesNotExistException(serviceInstanceId)
        }

    }

    override fun listFailas(): MutableIterable<Faila> = failaDAO.findAll()

    override fun findFailaAndResetNextFire(serviceInstanceId: String, nextFire: String, asgs: Set<String>): Faila {
        findFaila(serviceInstanceId)?.let {
            if (it.nextFire == nextFire && it.asgs == asgs) {
                it.nextFire = ""
                try {
                    failaDAO.save(it)
                } catch (conditionalCheckFail: ConditionalCheckFailedException) {
                    throw OutdatedScheduledFailaException()
                }
                return it
            }
        }

        throw OutdatedScheduledFailaException()
    }

    override fun setFailaNextFire(serviceInstanceId: String, nextFire: String) {
        findFaila(serviceInstanceId)?.let {
            it.nextFire = nextFire
            failaDAO.save(it)
        }
    }

    override fun findFaila(serviceInstanceId: String): Faila? = failaDAO.findById(serviceInstanceId).orElse(null)

}
