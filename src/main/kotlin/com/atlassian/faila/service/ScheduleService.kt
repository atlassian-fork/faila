package com.atlassian.faila.service

import com.amazon.sqs.javamessaging.message.SQSObjectMessage
import com.atlassian.faila.model.Faila
import com.atlassian.faila.servicebroker.instances.ServiceInstanceManager
import com.atlassian.faila.sqs.FailaMessageListener.FailaMessageType.SCHEDULE
import com.timgroup.statsd.StatsDClient
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod.GET
import org.springframework.web.bind.annotation.RestController
import javax.jms.Message
import javax.jms.MessageProducer

@RestController
@RequestMapping(value = ["faila"])
open class ScheduleService @Autowired constructor(
    private val serviceInstanceManager: ServiceInstanceManager,
    private val messageProducer: MessageProducer,
    private val statsDClient: StatsDClient
) {

    companion object {
        private val LOG = LoggerFactory.getLogger(ScheduleService::class.java)
    }

    @RequestMapping(value = ["schedule"], method = [GET])
    fun scheduleAll() {
        try {
            LOG.info("Syncing schedule for all existing failas")
            val existingFailas = serviceInstanceManager.listFailas()
            LOG.info("Found ${existingFailas.count()} failas to schedule")
            statsDClient.gauge("serviceinstances.total", existingFailas.count().toLong())
            existingFailas.forEach {
                sendMessageToScheduleFaila(it)
            }
        } catch (e: Exception) {
            LOG.error("Failed to scheduleAll existing failas", e)
        }
    }

    @RequestMapping(value = ["schedule/{service_id}"], method = [GET])
    open fun schedule(@PathVariable(name = "service_id") serviceId: String) =
        serviceInstanceManager.findFaila(serviceId)?.let {
            LOG.debug("Scheduling next execution for faila '$serviceId'")
            sendMessageToScheduleFaila(it)
        }

    private fun sendMessageToScheduleFaila(faila: Faila) {
        val scheduleMessage: Message = SQSObjectMessage(faila)
        scheduleMessage.setStringProperty("type", SCHEDULE.toString())
        messageProducer.send(scheduleMessage)
    }

}
